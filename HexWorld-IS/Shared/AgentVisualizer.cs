﻿using HexWorld_IS.HexWorld;
using Microsoft.AspNetCore.Components;
using ReenforcementLearning;
using SkiaSharp;
using Svg.Skia;
using System.Numerics;
using static HexWorld_IS.Pages.MainPage;

namespace HexWorld_IS.Shared
{
    class AgentVisualizer
    {
        private long currentFrame = 0;

        private long stepAnimate_startFrame = 0;
        private long stepAnimate_endFrame = 0;

        private Action<float>? stepAnimate_function = null;

        private Vector2 visualAgentPos = new(0, 0);

        private readonly AgentBase<HexWorldAction, HexWorldPos> agent;

        public bool IsAnimating { get; private set; }
        public long StepAnimDuration { get; set; } = 15;

        protected SKSvg svgAgent { get; private set; }

        public AgentVisualizer(SKSvg svgAgent, AgentBase<HexWorldAction,HexWorldPos> agent)
        {
            this.svgAgent = svgAgent;
            this.agent = agent;

            agent.StateTransition += SimulateStep;
        }

        private void SimulateStep(HexWorldPos previousPos, HexWorldPos nextPos, ResetInfo<HexWorldPos> resetInfo)
        {
            var pos0 = GetPos(previousPos);
            var pos1 = GetPos(nextPos);

            

            if (resetInfo.WasReset)
            {
                var posR = GetPos(resetInfo.ResetState);

                stepAnimate_function = (t) =>
                {
                    visualAgentPos =
                        Vector2.Lerp(pos0, pos1, t * t * (3f - 2f * t));

                    if (t == 1)
                        visualAgentPos = posR;
                };
            }
            else
                stepAnimate_function = (t) => visualAgentPos = Vector2.Lerp(pos0, pos1,
                    t * t * (3f - 2f * t));

            stepAnimate_startFrame = currentFrame;

            stepAnimate_endFrame = currentFrame + StepAnimDuration;

            IsAnimating = true;
        }

        private static Vector2 GetPos(HexWorldPos previousPos)
        {
            return previousPos.r * RightDir + previousPos.dr * DownRightDir;
        }

        public void Update()
        {
            if (currentFrame <= stepAnimate_endFrame)
            {
                var t = (currentFrame - stepAnimate_startFrame) / (float)(stepAnimate_endFrame - stepAnimate_startFrame);

                stepAnimate_function?.Invoke(t);

                if (currentFrame == stepAnimate_endFrame)
                    IsAnimating = false;
            }

            currentFrame++;
        }

        public void Draw(SKCanvas c, SKMatrix viewMatrix)
        {
            var pos = visualAgentPos;

            c.SetMatrix(viewMatrix);

            c.DrawCircle(pos.X, pos.Y, 40, new SKPaint
            {
                Color = new SKColor(0, 0, 0, 40),
                IsAntialias = true
            });

            c.SetMatrix(viewMatrix.PreConcat(
                SKMatrix.CreateTranslation(-50, -50)));

            c.DrawPicture(svgAgent.Picture, pos.X, pos.Y);
        }
    }
}
