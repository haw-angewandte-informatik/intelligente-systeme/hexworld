﻿using ReenforcementLearning;

namespace HexWorld_IS.HexWorld
{
    class Field
    {
        public FieldType Type { get; set; }

        public Field(FieldType type)
        {
            Type = type;
        }
    }

    enum HexWorldAction
    {
        GoUpRight,
        GoRight,
        GoDownRight,
        GoDownLeft,
        GoLeft,
        GoUpLeft,
    }

    enum FieldType
    {
        Normal,
        Goal
    }

    record struct HexWorldPos(int r, int dr);

    class World : IFiniteWorld<HexWorldAction, HexWorldPos>
    {
        public HexWorldPos? StartPos { get; set; }


        private readonly Dictionary<HexWorldPos, Field> fields = new();

        public IReadOnlyDictionary<HexWorldPos, Field> Fields => fields;

        public void AddField(Field field, HexWorldPos position)
        {
            fields.Add(position, field);
        }

        public void RemoveField(HexWorldPos position)
        {
            fields.Remove(position);
        }

        public (HexWorldPos newState, double reward) ExectuteAction(HexWorldPos state, HexWorldAction action)
        {
            HexWorldPos newState = action switch
            {
                HexWorldAction.GoUpRight   => new(state.r + 1, state.dr - 1),
                HexWorldAction.GoRight     => new(state.r + 1, state.dr + 0),
                HexWorldAction.GoDownRight => new(state.r + 0, state.dr + 1),
                HexWorldAction.GoDownLeft  => new(state.r - 1, state.dr + 1),
                HexWorldAction.GoLeft      => new(state.r - 1, state.dr + 0),
                HexWorldAction.GoUpLeft    => new(state.r + 0, state.dr - 1),
                _ => throw new NotImplementedException()
            };

            fields.TryGetValue(newState, out Field? field);

            float reward = field switch
            {
                null => -50, // Out
                { Type: FieldType.Goal} => 100, // Goal
                _ => 0 // No extra reward
            };

            return (newState, reward-5);
        }

        public HexWorldPos GetInititialState()
        {
            if (StartPos == null)
                throw new InvalidOperationException("StartField is not set, HexWorld is not properly initialized");

            return StartPos.Value;
        }

        public bool IsTerminalState(HexWorldPos state)
        {
            return !fields.TryGetValue(state, out var field) ||
                field.Type == FieldType.Goal;
        }

        public IEnumerable<HexWorldPos> GetAllStates() => fields.Keys;

        
    }
}