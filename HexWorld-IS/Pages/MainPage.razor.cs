﻿using HexWorld_IS.HexWorld;
using HexWorld_IS.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using ReenforcementLearning;
using SkiaSharp;
using SkiaSharp.Views.Blazor;
using Svg.Skia;
using System.Diagnostics;
using System.Numerics;


namespace HexWorld_IS.Pages;


//
// Summary:
//     Represents the SHIFT, ALT, and CTRL modifier keys on a keyboard.
[Flags]
public enum MouseModifiers
{
    None = 0,
    //
    // Summary:
    //     The left or right ALT modifier key.
    Alt = 1,
    //
    // Summary:
    //     The left or right SHIFT modifier key.
    Shift = 2,
    //
    // Summary:
    //     The left or right CTRL modifier key.
    Control = 4
}

public partial class MainPage
{
    const float MainViewWidth = 1200;
    const float MainViewHeight = 800;

    const float AgentViewWidth = 400;
    const float AgentViewHeight = 400;

    SKMatrix CameraMatrix = SKMatrix.CreateScale(0.5f, 0.5f);

    SKMatrix MainViewMatrix = SKMatrix.Empty;
    SKMatrix MainViewMatrixInv = SKMatrix.Empty;

    World world = new();

    AgentBase<HexWorldAction, HexWorldPos>? agent;

    AgentVisualizer? agentVisualizer;

    

    [Inject]
    HttpClient? Http { get; set; }

    SKSvg svgAgent = new();
    SKSvg svgGoalFlag = new();
    SKTypeface? nunito;

    public static Vector2 DownRightDir { get; private set; }
    public static Vector2 RightDir { get; private set; }

    public static SKPath Hexagon { get; private set; }

    static MainPage()
    {
        Hexagon = new SKPath();

        Hexagon.MoveTo(0, 100);

        for (int i = 1; i < 6; i++)
        {
            var (x, y) = MathF.SinCos(i * MathF.PI * 2 / 6f);

            Hexagon.LineTo(x * 100, y * 100);
        }

        var p0 = Hexagon.Points[0];
        var p1 = Hexagon.Points[1];

        DownRightDir = new Vector2(
            (p0.X + p1.X),
            (p0.Y + p1.Y)
        );

        RightDir = new Vector2(
            DownRightDir.Length(),
            0
        );

        Hexagon.Close();
    }

    


    void Init()
    {
        for (int i = -4; i < 4; i++)
            for (int j = -4; j < 4; j++)
            {
                var pos = DownRightDir * i + RightDir * j;

                if (pos.Length() > 501)
                    continue;

                var hexPos = new HexWorldPos(j, i);

                world.AddField(new Field(FieldType.Normal), hexPos);
            }

        var rng = new Random();

        var key = world.Fields.Keys.Skip(rng.Next(world.Fields.Count)).First();

        world.Fields[key].Type = FieldType.Goal;

        world.StartPos = new HexWorldPos(0, 0);

        agent = new SarsaAgent<HexWorldAction, HexWorldPos>(world);

        agentVisualizer = new AgentVisualizer(svgAgent, agent);
    }

    protected async override void OnInitialized()
    {
        Init();

        base.OnInitialized();

        svgAgent.Load(await Http!.GetStreamAsync($"assets/Agent.svg"));
        svgGoalFlag.Load(await Http!.GetStreamAsync($"assets/GoalFlag.svg"));
        nunito = SKTypeface.FromStream(await Http!.GetStreamAsync($"assets/Nunito.ttf"), 4);
    }

    private Vector2 mousePos = new(float.NaN);

    private bool leftButtonDown = false;
    private bool rightButtonDown = false;
    private bool middleButtonDown = false;

    private void OnMouseEvent(MouseEventArgs e)
    {
        mousePos = new((float)e.OffsetX, (float)e.OffsetY);

        leftButtonDown   = (e.Buttons & 0b001) > 0;
        rightButtonDown  = (e.Buttons & 0b010) > 0;
        middleButtonDown = (e.Buttons & 0b100) > 0;

        MouseModifiers = MouseModifiers.None;

        if (e.CtrlKey)
            MouseModifiers |= MouseModifiers.Control;

        if(e.ShiftKey)
            MouseModifiers |= MouseModifiers.Shift;

        if(e.AltKey)
            MouseModifiers |= MouseModifiers.Alt;
    }

    private void OnMouseClick(MouseEventArgs e)
    {
        
    }

    HexWorldPos hoveredFieldPos = new(int.MaxValue, int.MaxValue);

    private Vector2 size = new(float.NaN);

    private void OnMouseWheel(WheelEventArgs e)
    {
        //var mousePos = MainViewMatrixInv.MapPoint(this.mousePos.X, this.mousePos.Y);

        float scale = 1-(float)e.DeltaY*0.001f;

        //var undoMouseTranslation = SKMatrix.CreateTranslation(-mousePos.X, -mousePos.Y);
        //var redoMouseTranslation = SKMatrix.CreateTranslation(mousePos.X, mousePos.Y);

        CameraMatrix = CameraMatrix.PreConcat(SKMatrix.CreateScale(scale, scale));
    }

    private void OnMouseOut(MouseEventArgs e)
    {
        mousePos = new Vector2(float.NaN);

        hoveredFieldPos = new(int.MaxValue, int.MaxValue);
    }

    private readonly HashSet<string> heldKeys = new HashSet<string>();

    private MouseModifiers MouseModifiers = MouseModifiers.None;
    private void OnKeyDown(KeyboardEventArgs e)
    {
        heldKeys.Add(e.Key);
    }

    private void OnKeyUp(KeyboardEventArgs e)
    {
        heldKeys.Remove(e.Key);
    }

    Vector2 prevMousePos = new(float.NaN);

    private void Update()
    {
        var skPointPos = MainViewMatrixInv.MapPoint(mousePos.X, mousePos.Y);

        var pos = new Vector2(skPointPos.X, skPointPos.Y);

        var (diag, right) = GetClosetsFieldPos(pos);

        

        if (leftButtonDown && MouseModifiers==MouseModifiers.Alt)
        {
            CameraMatrix = CameraMatrix.PostConcat(CreateTranslation(mousePos - prevMousePos));
        }
        else
        {
            hoveredFieldPos = new(right, diag);
        }

        prevMousePos = mousePos;

        
        if (agentVisualizer!=null && !agentVisualizer.IsAnimating)
        {
            if (heldKeys.Contains("ArrowRight"))
            {
                agent?.Step();
            }
        }

        if (leftButtonDown)
        {
            bool fieldExists = world.Fields.TryGetValue(hoveredFieldPos, out var field);

            bool isProtected = 
                field?.Type == FieldType.Goal || 
                hoveredFieldPos == agent?.CurrentState ||
                hoveredFieldPos == world.GetInititialState();


            if (fieldExists && !isProtected && MouseModifiers==MouseModifiers.Shift)
                world.RemoveField(hoveredFieldPos);
            else if (!fieldExists && MouseModifiers == MouseModifiers.None)
                world.AddField(new Field(FieldType.Normal), hoveredFieldPos);
        }

        agentVisualizer?.Update();
    }

    private static SKMatrix CreateTranslation(Vector2 vec) => SKMatrix.CreateTranslation(vec.X, vec.Y);

    private void PaintMainView(SKPaintGLSurfaceEventArgs e)
    {
        Vector2 size = new(e.Info.Width, e.Info.Height);

        if (size != this.size)
        {
            MainViewMatrix = CameraMatrix.PostConcat(
                SKMatrix.CreateTranslation(MainViewWidth/2, MainViewHeight/2));

            if (!MainViewMatrix.TryInvert(out MainViewMatrixInv))
                Debugger.Break();

            MainViewMatrix = MainViewMatrix.PostConcat(
                SKMatrix.CreateScale(size.X / MainViewWidth, size.Y / MainViewHeight));


        }

        

        Update();



        var c = e.Surface.Canvas;
        c.Clear(SKColors.AliceBlue);


        //fields
        foreach (var (p, field) in world.Fields)
        {
            var pos = DownRightDir * p.dr + RightDir * p.r;

            c.SetMatrix(MainViewMatrix.PreConcat(CreateTranslation(
                pos
            )));

            c.DrawPath(Hexagon, new SKPaint()
            {
                Color = SKColors.LightGreen,
                IsAntialias = true
            });
            c.DrawPath(Hexagon, new SKPaint()
            {
                Color = SKColors.Green,
                IsStroke = true,
                StrokeWidth = 2,
                IsAntialias = true
            });

            if(field.Type==FieldType.Goal)
                c.DrawPicture(svgGoalFlag.Picture, -50, -50);
        }


        //hovered
        {
            var pos = DownRightDir * hoveredFieldPos.dr + RightDir * hoveredFieldPos.r;

            c.SetMatrix(MainViewMatrix.PreConcat(CreateTranslation(
                pos
            )));

            c.DrawPath(Hexagon, new SKPaint()
            {
                ColorF = new SKColorF(1,1,1,0.2f),
                IsAntialias = true
            });
            c.DrawPath(Hexagon, new SKPaint()
            {
                Color = SKColors.White,
                IsStroke = true,
                StrokeWidth = 4,
                StrokeJoin = SKStrokeJoin.Round,
                IsAntialias = true
            });
        }


        agentVisualizer?.Draw(c, MainViewMatrix);
    }

    private (int diag, int right) GetClosetsFieldPos(Vector2 pos)
    {
        var diagF = pos.Y / DownRightDir.Y;

        var rightF = (pos - DownRightDir * diagF).X / RightDir.X;

        int diag = (int)MathF.Floor(diagF);
        int right = (int)MathF.Floor(rightF);

        int closestDiag = int.MinValue;
        int closestRight = int.MinValue;
        var closestDist = float.MaxValue;

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
            {
                var _pos = (diag + i) * DownRightDir + (right + j) * RightDir;

                var dist = Vector2.DistanceSquared(_pos, pos);

                if (dist < closestDist)
                {
                    closestDist = dist;
                    closestDiag = (diag + i);
                    closestRight = (right + j);
                }
            }

        return (closestDiag, closestRight);
    }

    private void PaintAgentActionView(SKPaintGLSurfaceEventArgs e)
    {
        var c = e.Surface.Canvas;

        var size = e.Info.Size;

        if (agent == null || agent is not SarsaAgent<HexWorldAction, HexWorldPos> sarsaAgent)
            return;

        if (!sarsaAgent.QTable.TryGetValue(agent.CurrentState, out double[]? values))
            return;

        c.Clear(SKColors.White);

        var scaleMtx = SKMatrix.CreateScale(size.Width / AgentViewWidth, size.Height / AgentViewHeight);

        Vector2 center = new(AgentViewWidth / 2, AgentViewHeight / 2);

        c.SetMatrix(SKMatrix.CreateTranslation(center.X, center.Y).PostConcat(scaleMtx));



        double sum = values.Sum();

        for (int i = 0; i < 6; i++)
        {
            float a = (float)(values[i] / sum * 3);

            var path = SkiaSharp.Extended.SKGeometry.CreateSectorPath(i / 6f, (i + 1) / 6f, 60, 60 + 60 * a);

            var color = SKColorF.FromHsv(i * 40, 100, 100, 0.2f);

            c.DrawPath(path, new SKPaint
            {
                ColorF = color,
                IsAntialias = true
            });



            var (x, y) = MathF.SinCos((i + 0.5f) * MathF.PI * 2 / 6f);

            c.DrawText($"{values[i]:F2}", new SKPoint(x * 90, -y * 90), new SKPaint(
                new SKFont(nunito, 16))
            {
                ColorF = SKColorF.FromHsv(i * 40, 100, 50, 1f),
                TextAlign = SKTextAlign.Center,
                IsAntialias = true
            });
        }


        var svgSize = svgAgent.Picture!.CullRect.Size;

        c.SetMatrix(SKMatrix.CreateScaleTranslation(100f / svgSize.Width, 100f / svgSize.Height,
        center.X - 50, center.Y - 50).PostConcat(scaleMtx));
        c.DrawPicture(svgAgent.Picture, 0, 0);
    }
}