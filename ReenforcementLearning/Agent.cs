﻿namespace ReenforcementLearning
{
    public delegate void StateTransitionEventHandler<TState>(TState stateA, TState nextState, ResetInfo<TState> resetInfo) where TState : notnull;

    public record struct ResetInfo<TState>(bool WasReset, TState ResetState);

    public abstract class AgentBase<TAction, TState> 
        where TAction : struct, Enum
        where TState : notnull
    {
        public event StateTransitionEventHandler<TState>? StateTransition;

        public TState CurrentState { get; private set; }

        protected IWorld<TAction, TState> World { get; private set; }



        public AgentBase(IWorld<TAction, TState> world)
        {
            World = world;
            CurrentState = world.GetInititialState();
        }

        public void Step()
        {
            var previousState = CurrentState;

            CurrentState = ExecuteStep();

            bool isReset = World.IsTerminalState(CurrentState);

            TState stateAfterReset = World.GetInititialState();

            if (!CurrentState!.Equals(previousState))
                StateTransition?.Invoke(previousState, CurrentState, new(isReset, stateAfterReset));

            if (isReset)
                CurrentState = stateAfterReset;
        }

        protected abstract TState ExecuteStep();

        public void Run()
        {
            while(!World.IsTerminalState(CurrentState))
                Step();
        }
    }

    public class DumbAgent<TAction, TState> : AgentBase<TAction, TState> 
        where TAction : struct, Enum
        where TState : notnull
    {
        TAction[] actions;

        Random rng = new Random();

        public DumbAgent(IWorld<TAction, TState> world) : base(world)
        {
            actions = Enum.GetValues<TAction>();
        }

        protected override TState ExecuteStep()
        {
            var (newState, _) = World.ExectuteAction(
                CurrentState, 
                actions[rng.Next(actions.Length - 1)]
                );

            return newState;
        }
    }

    public class SarsaAgent<TAction, TState> : AgentBase<TAction, TState>
        where TAction : struct, Enum
        where TState : notnull
    {
        public float ExplorationValue { get; set; } = 0.1f;

        public float LearningRate { get; set; } = 0.1f;

        public float Discount { get; set; } = 1f;

        Dictionary<TState, double[]> qTable = new();

        public IReadOnlyDictionary<TState, double[]> QTable => qTable;

        TAction[] actions;

        Random rng = new Random();

        public SarsaAgent(IWorld<TAction, TState> world) : base(world)
        {
            actions = Enum.GetValues<TAction>();
        }

        private double[] GetOrCreateQTableRow(TState state)
        {
            if (!qTable.TryGetValue(state, out double[]? values))
            {
                values = new double[actions.Length];

                qTable[state] = values;
            }

            return values;
        }

        /// <summary>
        /// Polcy PI that chooses an action derived from a state, e.g. ε-greedy
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        private int ChooseAction(TState state)
        {
            float otherActionProbability = ExplorationValue / actions.Length;
            float bestActionProbability = 1 - (otherActionProbability * (actions.Length - 1));

            var row = GetOrCreateQTableRow(state);
            var max = row.Max();

            return row.Select((x,y)=>(value: x, index: y)).MaxBy(a => rng.Next( // rng rolls by using saved values
                    (int)((a.value == max ? bestActionProbability : otherActionProbability) * 100) // Compute probablilities for rng from saved values
                    )
                ).index; // return action the rng chose
        }

        int? nextChosen = null;

        protected override TState ExecuteStep()
        {

            int chosen = nextChosen ?? ChooseAction(CurrentState);

            var (newState, reward) = World.ExectuteAction(CurrentState, actions[chosen]);

            nextChosen = ChooseAction(newState);

            // magic
            var qNext = qTable[newState];
            var currentStateEntry = qTable[CurrentState];

            currentStateEntry[chosen] = 
                Reinforce(currentStateEntry[chosen], reward, qNext[nextChosen.Value]);

            return newState;
        }

        /// <summary>
        /// Calculate new value for chosen action.
        /// </summary>
        /// <param name="q">Current value in current state for chosen action</param>
        /// <param name="reward">Reward for taking the chosen action</param>
        /// <param name="qNext">Value in next state for next action</param>
        /// <returns>New value in current state for chosen action</returns>
        private double Reinforce(double q, double reward, double qNext)
        {
            return q + LearningRate * (reward + Discount * qNext - q);
        }
    }
}