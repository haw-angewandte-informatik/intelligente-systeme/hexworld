﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReenforcementLearning
{
    public interface IWorld<TAction, TState> where TAction : struct, Enum
    {
        TState GetInititialState();

        (TState newState, double reward) ExectuteAction(TState state, TAction action);

        bool IsTerminalState(TState state);
    }

    public interface IFiniteWorld<TAction, TState> : IWorld<TAction, TState> where TAction : struct, Enum
    {
        IEnumerable<TState> GetAllStates();
    }
}
